from datetime import datetime
from flask import Flask, render_template, request


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('main.html')


@app.route('/sugang', methods=['GET', 'POST'])
def sugang():
    number = request.form.get('number')
    time = datetime.now()
    return render_template('sugang.html', number=number, time=time)
